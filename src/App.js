import './App.css';
import { Button, Alert, Breadcrumb, Card } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Login from './views/Login';
import Profile from './views/Profile';
import Translator from './views/Translator';

function App() {

 
  
  return (
    <BrowserRouter>
    <div className="App">
      <Routes>
        <Route path="/" element={ <Login /> } />
        <Route path="/" element={ <Translator /> } />
        <Route path="/" element={ <Profile /> } />
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;
