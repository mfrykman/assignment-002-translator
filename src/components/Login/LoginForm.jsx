import { useForm } from "react-hook-form";

const usernameConfig = {
  required: true,
  minLength: 2,
};

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  console.log(errors);

  const errorMessage = (() => {
    if (!errors.username) {
        return null
    }

    if (errors.username.type === 'required') {
    return <span> Username is required </span>
    }

    if (errors.username.type === 'minLength') {
    return <span> Username must be longger </span>

    }
  })()

  return (
    <>
      <br></br><br></br><br></br><br></br>
      <h2>[Enter your name]</h2><br></br><br></br>
    
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <label htmlFor="username">
            Username: <br></br>
          </label><br></br>

          <input type="text" {...register("username", usernameConfig)} />
          <br></br><br></br>
          { errorMessage }
          </fieldset>
        <br></br>
        <button type="submit">Proceed</button>
        <br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br><br></br>
      </form>
    </>
  );
};

export default LoginForm;
